@extends('template')
@section('conteudo')
    <h4> Lançamento de Faltas </h4>
    <h4> Lançamento de Faltas do mês: {{$mesSelecionado}}</h4>
    @csrf
    <h5>
        <div class="form-group ">
            <label for="mes" class="col-sm-2 col-form-label">Selecione o mês: </label>
            <select name="meses" id="meses" class="col-sm-1">
                @foreach ($meses as $mes)
                    <option value={{$mes->mes}}>{{$mes->mes}}</option>
                @endforeach
            </select>
            <label for="mes" class="col-sm-2 col-form-label">Inserir novo mês: </label>
            <input type="number"  name="novoMes" id="novoMes" class="col-sm-1 border"/>
            <button type="button" id="inserirMes" class="btn btn-primary d-none">Inserir</button>
        </div>
    </h5>
    <input type="hidden" id="mesSelecionado" name="mesSelecionado" value="{{$mesSelecionado}}" /> 
    <div class="msg alert d-none"> </div>
    <table id="tbFaltas" class="table table-striped table-bordered">
        <tr>
            <th>Nome</th>
            <th>Turma</th>
            @foreach ($meses as $mes)
                <th>mês {{$mes->mes}}</th>
            @endforeach
            <th>mês {{$mesSelecionado}}</th>
        </tr>

        @foreach ($alunos as $aluno)
            <tr id="{{$aluno->id}}" class="faltas" >  
                <td><a href="{{ url('listaDados/'.$aluno->id)}}"> {{ $aluno->nome }} </a> </td>
                <td>{{ $aluno->turma }}</td>   
                @php 
                    $aux_falta = false; 
                    $faltas = $aluno->getFaltas();
                @endphp
                @foreach ($faltas as $key => $val)
                    <td> <input type="number" class="falta col-sm-2 border" name="{{$key}}" value="{{ $val }}" /> </td>   
                    @php $aux_falta = true; @endphp
                @endforeach
                @if (!$aux_falta)   
                    @foreach ($meses as $mes)
                        <td> <input type="number" class="falta col-sm-2 boreder" name="{{$mes}}" value='' /> </td>   
                    @endforeach
                @endif
                <td> <input type="number" class="falta col-sm-2 border" value="{{ $aluno->getFaltas($mesSelecionado) }}" /> </td>   
            </tr> 
        @endforeach
    </table>
    <br>
    <button type="button" id="salvar" class="btn btn-primary">Salvar dados</button>
    <button type="button" id="salvar" class="btn btn-primary">Salvar Faltas</button>
@stop
@section('rodape')
@stop

    jQuery(document).ready(function(){

        $("#novoMes").change(function(){
            $('#inserirMes').removeClass('d-none');
            $('.msg').addClass('d-none');
        });

        $("#inserirMes").click(function(){
            let mes = $(this).val();
            let meses = [];
            let opcoes = $("#meses option");

            for (let i=0; i< opcoes.length; i++){
                meses.push(opcoes[i].text);
            }

            //alert(meses);
            //alert($("#novoMes").val());
            if (meses.includes($("#novoMes").val())){
                $('.msg').removeClass('d-none').addClass('alert-danger').html("Mês: "+$("#novoMes").val()+" já existe na lista de meses!");
            }
            $(this).addClass('d-none');
        });

        $("#salvar").click(function(){
            //alert('salvar');

            var _token = $("input[name='_token']").val();
            let _token = $("input[name='_token']").val();
            //console.log('token: '+_token);

            var colunas = $("#tbFaltas tr th");
            //alert(colunas);
            var linhas = $("tr.faltas");
            let mesSelecionado = $("#mesSelecionado").val();
            //alert(mesSelecionado);
            let linhas = $("tr.faltas");
            linhas.each(function(){
                console.log($(this).attr('id'));
                console.log($(this).find('td').eq(0).text()); // nome
                console.log($(this).find('td').eq(1).text()); // turma
                console.log($(this).find('td input[name="8"]').val());
                console.log($(this).find('td input[name="9"]').val());
                console.log($(this).find('td').eq(2).val()); //faltas
                console.log($(this).find('td input').val());


                let id = $(this).attr('id');
                for (let coluna = 2; coluna < colunas.length; coluna++){
                    let mes = colunas.eq(coluna).text();
                    let faltas = $(this).find('td input[name="'+mes+'"]').val();
                    if (faltas=== undefined) {
                        faltas = 0;
                        mes = mes.split(' ');
                        mes = mes[1];
                    }
                let faltas = $(this).find('td input').val();
                
                $.ajax({
                    url: '{{ route("salvaFaltas")}}',
                    method: 'POST',
                    data: { _token: _token,
                        id_aluno : id, 
                        mes : mes, 
                        mes : mesSelecionado, 
                        faltas : faltas},
                    dataType: 'json',
                    success: function (data){
                        }
                    }
                });
                }
            });
        });
    });
